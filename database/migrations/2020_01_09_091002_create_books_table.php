<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('isbn')->unique();
            $table->text('image_url')->nullable();
            $table->text('downloaded_image')->nullable();
            $table->text('cropped_image')->nullable();
            $table->unsignedBigInteger('insert_bulk_id')->nullable();

            $table->foreign('insert_bulk_id')
                ->references('id')
                ->on('insert_bulks')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
