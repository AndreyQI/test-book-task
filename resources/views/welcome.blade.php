<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
    <div id="app">
        <books
            downloaded-image-base-path="{{ asset('storage/book_images') }}"
            cropped-image-base-path="{{ asset('storage/cropped_book_images') }}"
        ></books>
    </div>
    <script src="{{asset('js/app.js')}}" ></script>
    </body>
</html>
