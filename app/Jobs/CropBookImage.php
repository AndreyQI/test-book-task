<?php

namespace App\Jobs;

use App\Models\Book;
use App\Services\Books\CropBookImageService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CropBookImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Book
     */
    protected $book;

    /**
     * Create a new job instance.
     *
     * @param Book $book
     */
    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    /**
     * @param CropBookImageService $cropBookImageService
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(CropBookImageService $cropBookImageService)
    {
        // Check is not empty downloaded image
        if ($this->book->downloaded_image) {
            // Crop image
            $image = $cropBookImageService->crop($this->book);

            // Save cropped image
            $this->book->update(['cropped_image' => $image]);
        }
    }
}
