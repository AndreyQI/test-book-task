<?php

namespace App\Jobs;

use App\Models\Book;
use App\Services\Books\DownloadBookImageService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DownloadBookImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Book
     */
    protected $book;

    /**
     * Create a new job instance.
     *
     * @param Book $book
     */
    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    /**
     * Execute the job.
     *
     * @param DownloadBookImageService $downloadBookImageService
     * @return void
     */
    public function handle(DownloadBookImageService $downloadBookImageService)
    {
        // Check is not empty image url
        if ($this->book->image_url) {
            // Download image
            $image = $downloadBookImageService->download($this->book);

            // Save downloaded image
            $this->book->update(['downloaded_image' => $image]);
        }
    }
}
