<?php

namespace App\Services\Books;

use App\Models\Book;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CropBookImageService
{
    /**
     * @param Book $book
     * @param int $width
     * @param int $height
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function crop(Book $book, $width = 200, $height = 400): string
    {
        // open file a image resource
        $downloadedImage = Storage::disk('book_images')->get($book->downloaded_image);
        $img = Image::make($downloadedImage);

        // crop image
        $img->crop($width, $height);

        // save cropped image
        $croppedImageFileName = 'cropped_' . $book->downloaded_image;
        Storage::disk('cropped_book_images')->put($croppedImageFileName, $img->encode());

        return $croppedImageFileName;
    }
}
