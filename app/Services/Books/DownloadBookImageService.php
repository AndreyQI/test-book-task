<?php

namespace App\Services\Books;

use App\Models\Book;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DownloadBookImageService
{
    /**
     * Download book image
     *
     * @param Book $book
     * @return string
     */
    public function download(Book $book): string
    {
        $imageUrl = $book->image_url;
        $contents = file_get_contents($imageUrl);
        $name = Str::slug($book->title) . '-' . Str::slug(substr($imageUrl, strrpos($imageUrl, '/') + 1));
        Storage::disk('book_images')->put($name, $contents);

        return $name;
    }
}
