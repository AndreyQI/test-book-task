<?php

namespace App\Services\Books;

use DOMDocument;
use SimpleXMLElement;
use XMLReader;

class XmlBooksReaderService
{
    /**
     * Read xml file with books and return array with books data
     *
     * @param string $path
     * @return array
     */
    public function read(string $path): array
    {
        $books = [];
        $reader = new XMLReader;
        $reader->open($path);

        $document = new DOMDocument;

        // move to the first <book /> node
        while ($reader->read() && $reader->name !== 'book') ;

        while ($reader->name === 'book') {
            $node = simplexml_import_dom($document->importNode($reader->expand(), true));
            $books[] = $this->getBookData($node);

            // go to next <book />
            $reader->next('book');
        }

        return $books;
    }

    /**
     * Get book data
     *
     * @param SimpleXMLElement $node
     * @return array
     */
    private function getBookData(SimpleXMLElement $node)
    {
        return [
            'title' => (string)$node->title[0],
            'description' => (string)$node->description[0],
            'isbn' => (string)$node->ISBN[0],
            'image_url' => (string)$node->image[0]
        ];
    }
}
