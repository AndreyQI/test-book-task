<?php

namespace App\Services\Books;

use App\Jobs\CropBookImage;
use App\Jobs\DownloadBookImage;
use App\Models\Book;
use App\Models\InsertBulk;
use Carbon\Carbon;

class InsertBulkBooksService
{
    /**
     * Insert bulk books
     *
     * @param array $books
     * @param int $insertBulkSize
     */
    public function insert(array $books, int $insertBulkSize = 1000)
    {
        // Chunk books
        $chunkedBooks = array_chunk($books, $insertBulkSize);

        // Loop through chunked books
        foreach ($chunkedBooks as $booksToInsert) {
            // Create InsertBulk to know what books are inserted
            $insertBulk = InsertBulk::create();

            // Add InsertBulk to chunked books
            $booksToInsert = $this->addInsertBulkToChunkedBooks($booksToInsert, $insertBulk);

            // Insert books to database
            Book::insertIgnore($booksToInsert);

            // Get inserted books
            $insertedBooks = $this->getInsertedBooks($insertBulk);

            // Process inserted book images
            $this->processBookImages($insertedBooks);
        }
    }

    /**
     * Process book images
     *
     * @param $insertedBooks
     */
    public function processBookImages($insertedBooks)
    {
        foreach ($insertedBooks as $book) {
            $this->dispatchJobsToProcessBookImage($book);
        }
    }

    /**
     * Dispatch jobs to process book image
     *
     * @param Book $book
     */
    public function dispatchJobsToProcessBookImage(Book $book)
    {
        DownloadBookImage::withChain([
            new CropBookImage($book),
        ])->dispatch($book);
    }

    /**
     * Add InsertBulk to chunked books array
     * to know what books will be inserted in database
     *
     * @param array $books
     * @param InsertBulk $insertBulk
     * @return array
     */
    private function addInsertBulkToChunkedBooks(array $books, InsertBulk $insertBulk)
    {
        $books = array_map(function ($item) use ($insertBulk) {
            $item['insert_bulk_id'] = $insertBulk->id;
            $item['created_at'] = Carbon::now();
            $item['updated_at'] = Carbon::now();
            return $item;
        }, $books);

        return $books;
    }

    /**
     * Get inserted books by insert_bulk_id
     *
     * @param InsertBulk $insertBulk
     * @return mixed
     */
    private function getInsertedBooks(InsertBulk $insertBulk)
    {
        return Book::where('insert_bulk_id', $insertBulk->id)->get();
    }
}
