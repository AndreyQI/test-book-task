<?php

namespace App\Console\Commands\Books;

use App\Services\Books\InsertBulkBooksService;
use App\Services\Books\XmlBooksReaderService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class InsertBulk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:insert-bulk {--storage-xml-file=} {--bulk-size=1000}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read xml file and insert bulk books';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param XmlBooksReaderService $xmlBooksReaderService
     * @param InsertBulkBooksService $insertBulkBooksService
     * @return mixed
     */
    public function handle(
        XmlBooksReaderService $xmlBooksReaderService,
        InsertBulkBooksService $insertBulkBooksService
    )
    {
        $storageXmlFile = $this->option('storage-xml-file');
        $bulkSize = $this->option('bulk-size');

        // Get books
        $booksXmlPath =storage_path('app/public/books_files') . '/' . $storageXmlFile;
        $books = $xmlBooksReaderService->read($booksXmlPath);

        // Insert bulk books
        $insertBulkBooksService->insert($books, $bulkSize);
    }
}
