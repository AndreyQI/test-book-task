<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;

class Book extends Model
{
    use InsertOnDuplicateKey;

    protected $table = 'books';
    protected $fillable = [
        'title',
        'description',
        'isbn',
        'image_url',
        'downloaded_image',
        'cropped_image',
        'insert_bulk_id'
    ];
}
